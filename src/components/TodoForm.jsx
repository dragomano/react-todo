import { useRef } from 'react';

function TodoForm(props) {
  const inputRef = useRef(null)

  const addTodo = () => {
    props.onSubmit(inputRef.current.value)
    inputRef.current.value = ''
    inputRef.current.focus()
  };

  return (
    <div className="p-4 bg-gray-100">
      <div className="flex items-center">
        <input
          ref={inputRef}
          type="text"
          className="flex-1 mr-2 py-2 px-4 rounded-md border border-gray-300"
          placeholder="Новая задача"
          autoFocus
        />
        <button
          className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md"
          onClick={addTodo}
        >
          Добавить
        </button>
      </div>
    </div>
  )
}

export default TodoForm
